///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//  Reference time: Tue Jan 21 04:26:07 PM HST 2014 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 8 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 9 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 10 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 11 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 12 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 13 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 14 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 15 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 16 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 17 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 18 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 19 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 20 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 21 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 22 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 23 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 24 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 25 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 26 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 27 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 28 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 29 
//  Years: 8 Days: 35 Hours: 1 Minutes: 18 Seconds: 30 
//
// @author Kobe Uyeda <kobek@hawaii.edu>
// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>

int main() {
   const int SECONDS_IN_A_YEAR  = 31536000;
   const int SECONDS_IN_A_DAY   = 86400;
   const int SECONDS_IN_A_HOUR  = 3600;
   const int SECONDS_IN_A_MINUTE = 60;

   struct tm tm_Reference_Time;        // This is the tm decleration for Reference time
   time_t todayTime;                   // Declaring Todays Time
   time_t referenceTimeInSeconds;      // Declaring the tm reference Time variable that will hold it in the time_t format
   double differenceOfReferenceAndNow; // Declaring the difference variable that will store the difference from reference to the time now
   int years;
   int days;
   int hours;
   int minutes;
   int seconds;

   // This is the reference time in the tm structure
   tm_Reference_Time.tm_sec   = 7;
   tm_Reference_Time.tm_min   = 26;
   tm_Reference_Time.tm_hour  = 16;
   tm_Reference_Time.tm_mday  = 21;
   tm_Reference_Time.tm_mon   = 0;
   tm_Reference_Time.tm_year  = 114;
   tm_Reference_Time.tm_isdst = -1;
   tm_Reference_Time.tm_wday  = 2;

   // Takes the tm structure and converts it into seconds
   referenceTimeInSeconds = mktime(&tm_Reference_Time);
   //Original Print Reference Time Since the format was wanted this way
   //printf("Reference time: Tue Jan 21 04:26:07 PM HST 2014 \n");
   printf("Reference time: %s", ctime(&referenceTimeInSeconds));
   // While loop to print the difference in time from referenceTime every second
   while(true){
      //fflush(stdout);
      //sleep(1);
      
      todayTime = time(NULL);
      
      differenceOfReferenceAndNow = difftime(todayTime, referenceTimeInSeconds);
      
      years = differenceOfReferenceAndNow / SECONDS_IN_A_YEAR;
      differenceOfReferenceAndNow = (int)differenceOfReferenceAndNow % SECONDS_IN_A_YEAR;
      days = differenceOfReferenceAndNow / SECONDS_IN_A_DAY;
      differenceOfReferenceAndNow = (int)differenceOfReferenceAndNow % SECONDS_IN_A_DAY;
      hours = differenceOfReferenceAndNow / SECONDS_IN_A_HOUR;
      differenceOfReferenceAndNow = (int)differenceOfReferenceAndNow % SECONDS_IN_A_HOUR;
      minutes = differenceOfReferenceAndNow / SECONDS_IN_A_MINUTE;
      differenceOfReferenceAndNow = (int)differenceOfReferenceAndNow % SECONDS_IN_A_MINUTE;
      seconds = differenceOfReferenceAndNow;

      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d \n", years, days, hours, minutes, seconds);
      sleep(1);
   }
   return 0;
}
